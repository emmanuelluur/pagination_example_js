require('dotenv').config()
const express = require('express');
const path = require("path");
const formidableMiddleware = require('express-formidable');
const router = require("./rutas/router");

const app = express();
const PORT = process.env.PORT_SERVER || 3000;


/**
 * set views
 */
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
/** 
 * uso de carpeta public para archivos locales
*/
app.use('/public', express.static(__dirname + '/public'));

/**
 * formdata Config
 */
// uso de formData
app.use(formidableMiddleware({
    encoding: 'utf-8',
    uploadDir: './public', // file to upload
    multiples: true, // req.files to be arrays of files
}));


/***** */
app.use("/", router);
// error
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next();
});
// error handler
// define as the last app.use callback
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.send(err.message);
});


app.listen(PORT, () => {
    
    console.log(`Server run PORT ${PORT}`);
})
