const express = require("express");
const router = express.Router();
const post = require("../app/controllers/postController");
/**
 * Index, se toma como vista de blog
 */
router.get("/", post.getPost);
/**
 * GET paginacion
 */
router.get("/paginate/:ident", post.getPost);
/**
 * si en paginacion no se encuentra un ident para paginar
 * devuelve los post iniciales
 */
router.get("/paginate/", post.getPost);
/**
 * Formularios
 */
router.get("/post/nuevo", post.newPost);


/**
 * post
 */
router.post("/post/nuevo", post.createPost);
/**
 * no rutas
 */
router.get("*", (req,res)=> {
    res.send("Recurso no encontrado :(")
})



module.exports = router;