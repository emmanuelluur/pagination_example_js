const Post = require("../models/postModel");
const htmlspecialchars = require('htmlspecialchars');
const Moment = require('moment-timezone');

exports.getPost = (req, res) => {
    Post.all()
        .then(results => {
            let resp = JSON.parse(results)
            let count = resp.length;
            if (results != null) {
                Post.getPag((req.params.ident -1) * 3 || 0) // el resultado se resta 1 y semultiplica por el LIMITE de post a mostrar
                    .then(results => {
                        let resp = JSON.parse(results)
                        if (results != null) {
                            // se divide total de registros entre el limite mostrado, estos seran las paginaciones
                            res.render("index", { title: "Nuevo Post", datos: resp, total: (count / 3) }) 
                        }
                    })
            } else {
                res.render("index", { title: "Nuevo Post", datos: [], total: 0 })
            }

        })
        .catch(err => {
            res.render("index", { title: "Nuevo Post", datos: [], total: 0 })
        })

}


exports.newPost = (req, res) => {
    res.render("post/registro", { title: "Nuevo Post" })
}

exports.createPost = (req, res) => {
    let nDate = Moment().tz('America/Matamoros').format();
    let data = {
        title: htmlspecialchars(req.fields.titulo_blog),
        post: htmlspecialchars(req.fields.post_content),
        post_create: nDate,
        created_at: nDate,
        updated_at: nDate
    }
    Post.save(data)
        .then(() => {
            res.send({ code: "success", class: "alert-success", message: "Post publicado" })
        })
        .catch((err) => {
            res.send({ code: "error", class: "alert-danger", message: err })
        })
}