const post= require("../../config");
const table = "tbl_posts";
/**
 * Guarda los datos de usuario
 */
exports.save = (data) => {
    return new Promise((resolve, reject) => {
        post.query(`INSERT INTO ${table} SET ?`, data, function (error, results, fields) {
            if (error)  reject(error);
            //  console.log(results);
            resolve();
        });
    })
}

/**
 * todos los post
 */

 exports.all=() => {
    return new Promise((resolve, reject) => {
        post.query(`SELECT * FROM ${table} WHERE active = 1`, function (error, results, fields) {
            if (error)  reject(error);
            console.log(Object.keys(results));
            resolve(JSON.stringify(results));
        });
    })
 }

 /**
 * POSTS BY LIMIT
 */

exports.getPag=(ident) => {
    console.log(ident)
    return new Promise((resolve, reject) => {
        post.query(`SELECT * FROM ${table} WHERE active = 1 AND id > ${ident} LIMIT 3`, function (error, results, fields) {
            if (error)  reject(error);
            console.log(JSON.stringify(results));
            resolve(JSON.stringify(results));
        });
    })
 }
