const content = document.getElementById("my_app");

document.getElementById("my_app").addEventListener("click", el => {
    if(el.target.getAttribute("id") == 'btn_publicar') {
        document.getElementById("message").innerHTML= "";
        let form = document.getElementById("registra_post");
        let data = new FormData(form);
        post("/post/nuevo", data)
        .then(res=>{
            let resp = JSON.parse(res);
            document.getElementById("message").innerHTML= `<div class = 'alert ${resp.class}'>${resp.message}</div>`;
            //form.reset();
        })
        .catch(err => {
            
            document.getElementById("message").innerHTML= `<div class = 'alert alert-danger>${err}</div>`;
            form.reset();
        })
    }
})