CREATE DATABASE IF NOT EXISTS blog_app DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish2_ci;
USE blog_app;

CREATE TABLE IF NOT EXISTS tbl_posts
(
    id BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT, -- primary key column
    title VARCHAR(60),
    post TEXT,
    post_create DATE,
    active TINYINT(1) DEFAULT 1,
    created_at DATETIME,
    updated_at DATETIME
    -- specify more columns here
)ENGINE=InnoDB DEFAULT CHARSET=utf8;